﻿using System;
using UnityEngine;

namespace Figma2Unity
{
    public interface IFigma2UnityManager
    {
        void UpdateToken(string token);
    }

    public interface IFigma2UnityManagerInternal : IFigma2UnityManager
    {
        void FetchImage(string figmaID, Action onComplete);
    }

    public class Figma2UnityManager : MonoBehaviour, IFigma2UnityManagerInternal
    {
        private string figmaToken;

        //Create is a lie.. shhhhhh..
        private static IFigma2UnityManagerInternal instance;
        internal static IFigma2UnityManagerInternal Instance
        {
            get
            {
                if (instance == null)
                {
                    var gameObject = new GameObject("Figma2UnityManager");
                    DontDestroyOnLoad(gameObject);

                    instance = gameObject.AddComponent<Figma2UnityManager>();
                }
                return instance;
            }
        }
        /// <summary>
        /// Get the token from figma, see here for more details:
        /// https://www.figma.com/developers/api#access-tokens
        /// </summary>
        /// <param name="token">The figma token</param>
        public static IFigma2UnityManager Create(string token) 
        {
            if(string.IsNullOrWhiteSpace(token))
            {
                Debug.LogError("Figma token cannot be empty");
                return null;
            }

            Instance.UpdateToken(token);
            
            return Instance;
        }

        public void UpdateToken(string newToken)
        {
            figmaToken = newToken;
        }

        public void FetchImage(string figmaID, Action onComplete)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System.Collections;
using UnityEngine;
using Unity.EditorCoroutines.Editor;

namespace Figma2Unity
{
    public class CoroutineRunner : MonoBehaviour
    {
        private static CoroutineRunner instance;
        private static CoroutineRunner Instance
        {
            get
            {
                if(instance == null)
                {
                    var gameObject = new GameObject("RoutineRunner");
                    DontDestroyOnLoad(gameObject);
                    instance = gameObject.AddComponent<CoroutineRunner>();
                }

                return instance;
            }
        }

        public static void RunRoutine(IEnumerator enumerator, object routineOwner)
        {
            if (Application.isPlaying)
                Instance.StartCoroutine(enumerator);
            else
                EditorCoroutineUtility.StartCoroutine(enumerator, routineOwner);
        }

        public static void StopRoutine(IEnumerator enumerator)
        {
            if (enumerator == null)
                return;

            if (Application.isPlaying)
                Instance.StopCoroutine(enumerator);
            //else
              //  EditorCoroutineUtility.StopCoroutine(enumerator);
        }
    }
}
﻿using System.Collections;
using UnityEngine.Networking;
using System.Collections.Concurrent;
using System.Linq;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace Figma2Unity
{
    public class WebRequestProcessor
    {
        private ConcurrentQueue<RequestJob> jobQueue = new ConcurrentQueue<RequestJob>();
        private IEnumerator routine;
        public event Action OnDone;
        ~WebRequestProcessor()
        {
            CoroutineRunner.StopRoutine(routine);
            routine = null;
        }

        public void ProcessWebRequest(params RequestJob[] jobs)
        {
            foreach (var job in jobs)
                jobQueue.Enqueue(job);

            if(routine == null)
            {
                routine = JobProccesor();
                CoroutineRunner.RunRoutine(routine, this);
            }
        }

        private IEnumerator JobProccesor()
        {
            while (jobQueue.Count != 0)
            {
                if (jobQueue.TryDequeue(out var job) == false)
                    continue;

                string url;
                switch (job.JobType)
                {
                    case JobType.FetchPngUrl:
                        url = $"https://api.figma.com/v1/images/ {job.FigmaProjectKey}?ids={job.FigmaID}&scale={job.Scale.ToString("0.00")}&format=png".Replace(" ", "");
                        break;
                    case JobType.FetchRawPNG:
                        url = job.RawURL;
                        break;
                    case JobType.Files:
                        url = $"https://api.figma.com/v1/files/" + job.FigmaProjectKey;
                        break;

                    default:
                        continue;
                }

                var www = UnityWebRequest.Get(url);
                www.SetRequestHeader("X-FIGMA-TOKEN", job.FigmaToken);
                var operation = www.SendWebRequest();

                while (operation.isDone == false)
                    yield return null;

                switch (job.JobType)
                {
                    case JobType.FetchPngUrl:
                        job.JobType = JobType.FetchRawPNG;
                        job.RawURL = ParseImageResponse(www.downloadHandler.text);
                        jobQueue.Enqueue(job);
                        break;
                    case JobType.FetchRawPNG:
                        job?.OnComplete(string.Empty, www.downloadHandler.data, job.Scale, null);
                        break;
                    case JobType.Files:
                        var reponse = ParseFilesResponse(www.downloadHandler.text);
                        job?.OnComplete(null, null, 0, reponse);
                        break;
                }
                yield return null;
                www.Dispose();
            }

            routine = null;
            OnDone?.Invoke();
        }

        //I want newton soft
        private static string ParseImageResponse(string rawJson)
        {
            var tokens = rawJson.Split('\"').ToList();
                
            var errIndex = tokens.FindIndex(s => s.Equals("err"));
            var err = tokens[errIndex + 1];
            if (err.Contains("null") == false)
            {
                Debug.LogError(err);
                return null;
            }
                
            return tokens.FirstOrDefault(s => s.StartsWith("http"));
        }

        //I want newton soft
        private static List<(string, string)> ParseFilesResponse(string rawJson)
        {
            if (rawJson.Contains("err\""))
            {
                Debug.LogError("Error in response from backend: Respones" + Environment.NewLine + rawJson);
                return null;
            }

            var tokens = rawJson.Split('\"').ToList();
            var pairs = tokens.Select((line, index) => (line, index))
                              .Where(tuple => tuple.line.StartsWith("id"))
                              .Select(tuple => (tokens[tuple.index + 2], tokens[tuple.index + 6]));

            return pairs.ToList();
        }
    }
}
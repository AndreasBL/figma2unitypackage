﻿using System;
using System.Collections.Generic;

namespace Figma2Unity
{
    public enum JobType { FetchPngUrl, FetchRawPNG, Files }
    public class RequestJob
    {
        public string FigmaToken;
        public string FigmaProjectKey;
        public string FigmaID;
        public string UnityProjectPath;
        public float Scale;
        public JobType JobType;
        public string RawURL;
        public Action<string, byte[], float, List<(string, string)>> OnComplete;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Figma2Unity
{
    [RequireComponent(typeof(Image))]
    public class FigmaImage : MonoBehaviour
    {
        [Header("Figma settings")]
        [SerializeField] private string figmaId;
        
        private Image image;
        protected void Start()
        {
            image = GetComponent<Image>();
        }
    }
}
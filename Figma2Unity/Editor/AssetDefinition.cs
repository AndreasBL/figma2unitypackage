﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Figma2Unity.Editor
{
    [Serializable]
    public class AssetDefinition
    {
        public string DisplayName;
        public string FigmaID;
        public string FigmaProjectID;
        public string UnityProjectPath;
        public List<float> Scales;
        [NonSerialized]
        public bool selected;
    }

    [Serializable]
    public class AssetDefinitionContainer
    {
        public List<AssetDefinition> AssetDefinitions;
    }
}
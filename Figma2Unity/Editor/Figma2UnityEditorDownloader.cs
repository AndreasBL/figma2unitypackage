﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Figma2Unity.Editor
{
    public static class Figma2UnityEditorDownloader
    {
        public static void FetchSelected(Figma2UnityEditorModel model, WebRequestProcessor webRequestProcessor)
        {

            var jobs = model.AssetDefinitions.Where(d => d.selected)
                                             .Select(assetDefinition => AssetToJobs(assetDefinition, model, (text, bytes, scale, _) => OnDownloadComplete(text, bytes, scale, assetDefinition)))
                                             .SelectMany(l => l)
                                             .ToArray();

            webRequestProcessor.ProcessWebRequest(jobs);
        }

        private static void OnDownloadComplete(string text, byte[] rawImage, float scale, AssetDefinition assetDefinition)
        {
            var filePath = Application.dataPath + "/" + assetDefinition.UnityProjectPath + "x" + scale + ".png";
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            File.WriteAllBytes(filePath, rawImage);
            AssetDatabase.Refresh();
            Debug.Log("Done writing: " + filePath);
        }

        private static IEnumerable<RequestJob> AssetToJobs(AssetDefinition assetDefinition, Figma2UnityEditorModel model, Action<string, byte[], float, List<(string, string)>> onComplete)
        {
            RequestJob NewJob(float scale)
            {
                return new RequestJob()
                {
                    Scale = scale,
                    FigmaToken = model.FigmaToken,
                    FigmaProjectKey = assetDefinition.FigmaProjectID,
                    FigmaID = assetDefinition.FigmaID,
                    UnityProjectPath = assetDefinition.UnityProjectPath,
                    JobType = JobType.FetchPngUrl,
                    OnComplete = onComplete
                };
            }

            return assetDefinition.Scales.Select(NewJob);
        }
    }
}
﻿using System.Collections.Generic;
using System;
using UnityEngine;

namespace Figma2Unity.Editor
{
    [Serializable]
    public class Figma2UnityEditorModel : ISerializationCallbackReceiver
    {
        [NonSerialized]
        public List<AssetDefinition> AssetDefinitions;
        public string DefinitionPath = "";
        public string FigmaToken = "";
        public string DefaultFigmaProejctID = "";
        public List<float> DefaultScales;

        public void OnAfterDeserialize()
        {
            if (DefaultScales == null || DefaultScales.Count == 0)
                DefaultScales = new List<float> { 1f, 2f, 3f };
        }

        public void OnBeforeSerialize()
        {
            
        }
    }
}
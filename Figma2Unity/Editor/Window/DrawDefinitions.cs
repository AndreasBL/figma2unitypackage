﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Figma2Unity.Editor.View
{
    public static partial class Views
    {
        private static int foldoutIndex = 1;
        public static bool DrawDefinitions(Figma2UnityEditorModel model, Action<Figma2UnityEditorModel> save, Func<List<AssetDefinition>> loadDefinitions, Action<AssetDefinition> inspectDefinition)
        {
            var fetchSelected = false;

            EditorGUILayout.LabelField("Figma Assets", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {

                    if (GUILayout.Button("Add new figma asset"))
                        model.AssetDefinitions.Insert(0, new AssetDefinition() { FigmaProjectID = model.DefaultFigmaProejctID });

                    if (GUILayout.Button("Save"))
                        save(model);

                    if (GUILayout.Button("Reload from disk"))
                        model.AssetDefinitions = loadDefinitions();

                    if (GUILayout.Button("Fetch selected"))
                        fetchSelected = true;

                    if (GUILayout.Button("Fetch all"))
                    {
                        model.AssetDefinitions.ForEach(a => a.selected = true);
                        fetchSelected = true;
                    }

                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

                var definitions = model.AssetDefinitions;
                var deleteIndex = -1;
                for (int i = 0; i < definitions.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    
                        var foldout = EditorGUILayout.Foldout(foldoutIndex == i, definitions[i].DisplayName);

                        EditorGUILayout.LabelField("Select");
                        definitions[i].selected = EditorGUILayout.Toggle(definitions[i].selected);
                    
                    EditorGUILayout.EndHorizontal();

                    if (foldout)
                        foldoutIndex = i;
                    else continue;

                    if (DrawDefinitionDetails(definitions[i]))
                        deleteIndex = i;
                }

                //Side effect... *sigh*
                if (deleteIndex != -1)
                    definitions.RemoveAt(deleteIndex);
            }
            EditorGUILayout.EndVertical();

            return fetchSelected;
        }

        private static bool DrawDefinitionDetails(AssetDefinition definition)
        {
            var delete = false;

            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField("Editing " + definition.DisplayName, EditorStyles.boldLabel);

                EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Name");
                    definition.DisplayName = EditorGUILayout.TextField(definition.DisplayName);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Figma Project Key");
                    definition.FigmaProjectID = EditorGUILayout.TextField(definition.FigmaProjectID);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Figma ID");
                    definition.FigmaID = EditorGUILayout.TextField(definition.FigmaID);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField("Scales");
                    if (GUILayout.Button("Add scale"))
                        definition.Scales.Add(1f);

                    GUI.enabled = definition.Scales.Count > 1;
                    if (GUILayout.Button("Remove scale"))
                        definition.Scales.RemoveAt(definition.Scales.Count - 1);
                    GUI.enabled = true;

                    for (int i = 0; i < definition.Scales.Count; i++)
                        definition.Scales[i] = EditorGUILayout.FloatField(definition.Scales[i]);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Unity project path");
                    definition.UnityProjectPath = EditorGUILayout.TextField(definition.UnityProjectPath);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Delete");
                    if (GUILayout.Button("X"))
                        delete = true;
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();

            return delete;
        }
    }
}
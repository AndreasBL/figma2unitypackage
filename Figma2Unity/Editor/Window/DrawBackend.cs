﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Figma2Unity.Editor.View
{
    public static partial class Views
    {
        private static List<(string id, string name)> backendData = new List<(string, string)>();
        private static GUILayoutOption width = GUILayout.MaxWidth(300);
        private static Vector2 scrollPosition = Vector2.zero;
        private static string filter = string.Empty;
        public static void DrawBackend(Figma2UnityEditorModel model, WebRequestProcessor webRequestProcessor)
        {

            EditorGUILayout.LabelField("Backend (ONLY WORKS FOR SMALL PROJECTS)", EditorStyles.boldLabel);
            if(GUILayout.Button("Fetch"))
            {
                var job = new RequestJob()
                {
                    JobType = JobType.Files,
                    FigmaToken = model.FigmaToken,
                    FigmaProjectKey = model.DefaultFigmaProejctID,
                    OnComplete = (a, b, c, data) => backendData = data
                };

                webRequestProcessor.ProcessWebRequest(job);
            }

            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.Label("Filter on name: ");
                filter = GUILayout.TextField(filter);
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginVertical("Box");
            {
                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
                {
                    for (int i = 0; i < backendData.Count; i++)
                    {
                        if (backendData[i].name.Contains(filter) == false)
                            continue;

                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("Name: " + backendData[i].name, width);
                            GUILayout.Label("Id: " + backendData[i].id, width);
                            if(GUILayout.Button("Add: " + backendData[i].name))
                            {
                                var definition = new AssetDefinition()
                                {
                                    DisplayName = backendData[i].name,
                                    FigmaID = backendData[i].id,
                                    FigmaProjectID = model.DefaultFigmaProejctID,
                                    Scales = model.DefaultScales,
                                    UnityProjectPath = backendData[i].name.Replace(" ", "")
                                };
                                model.AssetDefinitions.Add(definition);
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }
    }
}
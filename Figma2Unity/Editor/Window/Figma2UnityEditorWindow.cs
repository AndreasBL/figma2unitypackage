﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;
using System;
using Figma2Unity.Editor.View;

namespace Figma2Unity.Editor
{
    public class Figma2UnityEditorWindow : EditorWindow
    {
        
        private const string figmaModelKey = "FigmaModelEditor";
        
        private Figma2UnityEditorModel model;
        private WebRequestProcessor webRequestProcessor;
        private bool guiEnabled = true;

        private int toolbarInt = 0;
        private string[] toolbarStrings = { "In Project", "Settings", "Backend" };

        [MenuItem("Window/Figma 2 Unity")]
        private static void Init()
        {
            var window = EditorWindow.GetWindow<Figma2UnityEditorWindow>(typeof(Figma2UnityEditorWindow));
            window.LoadEditorWindowState();
            window.Show();
        }

        //Happens after each recompie in unity
        private void OnValidate()
        {
            LoadEditorWindowState();
        }

        private void LoadEditorWindowState()
        {
            webRequestProcessor = new WebRequestProcessor();
            webRequestProcessor.OnDone += () =>
            {
                guiEnabled = true;
                Repaint();
            };

            var rawModel = EditorPrefs.GetString(figmaModelKey, defaultValue: null);
            if (string.IsNullOrWhiteSpace(rawModel))
                model = new Figma2UnityEditorModel();
            else
                model = JsonUtility.FromJson<Figma2UnityEditorModel>(rawModel);

            model.DefinitionPath = Application.dataPath + "/FigmaDefinitions.json";
            model.AssetDefinitions = Load(model.DefinitionPath) ?? new List<AssetDefinition>();
        }

        private void OnDisable()
        {
            Save(model);
        }


        private static void Save(Figma2UnityEditorModel model)
        {
            var container = new AssetDefinitionContainer() { AssetDefinitions = model.AssetDefinitions };
            var rawJson = JsonUtility.ToJson(container, prettyPrint: true);

            File.WriteAllText(model.DefinitionPath, rawJson);
            AssetDatabase.Refresh();
            Debug.Log("Saved Figma definitions to: " + model.DefinitionPath);

            var rawModel = JsonUtility.ToJson(model);
            EditorPrefs.SetString(figmaModelKey, rawModel);
            Debug.Log("Saved editor state to EditorPrefs");
        }

        private static List<AssetDefinition> Load(string path)
        {
            if (File.Exists(path) == false)
                return null;
            
            var rawJson = File.ReadAllText(path);
            var container = JsonUtility.FromJson<AssetDefinitionContainer>(rawJson);
            
            return container.AssetDefinitions;
        }

        private AssetDefinition inspectedDefinition = null;

        private void OnGUI()
        {
            GUI.skin.textField.alignment = TextAnchor.MiddleRight;
            if (model == null)
                LoadEditorWindowState();

            GUI.enabled = guiEnabled;

            EditorGUILayout.Separator();
            toolbarInt = GUILayout.Toolbar(toolbarInt, toolbarStrings);
            EditorGUILayout.Separator();

            switch(toolbarInt)
            {
                case 0:
                    if (Views.DrawDefinitions(model, Save, () => Load(model.DefinitionPath), selected => inspectedDefinition = selected))
                    {
                        guiEnabled = false;
                        Figma2UnityEditorDownloader.FetchSelected(model, webRequestProcessor);
                    }
                    break;

                case 1:
                    Views.DrawSettings(model);
                    break;

                case 2:
                    Views.DrawBackend(model, webRequestProcessor);
                    break;


            }
        }
    }
}
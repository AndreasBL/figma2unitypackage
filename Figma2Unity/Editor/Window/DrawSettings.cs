﻿using UnityEditor;
using UnityEngine;

namespace Figma2Unity.Editor.View
{
    public static partial class Views
    {
        private static float newScale = 1f;
        public static void DrawSettings(Figma2UnityEditorModel model)
        {
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical("Box");
            {
                EditorGUILayout.BeginHorizontal("Box");
                {
                    GUILayout.Label("Figma token:");
                    model.FigmaToken = GUILayout.PasswordField(model.FigmaToken, '*');
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal("Box");
                {
                    GUILayout.Label("Default Figma project Key:");
                    model.DefaultFigmaProejctID = GUILayout.TextField(model.DefaultFigmaProejctID);
                }
                EditorGUILayout.EndHorizontal();

            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.LabelField("Default Scales", EditorStyles.boldLabel);
            EditorGUILayout.BeginVertical("Box");
            {
                EditorGUILayout.LabelField("Scales added for new figma items, Figma provide scales in 2 decimal floats e.g. 2.34");
                EditorGUILayout.BeginHorizontal("Box");
                {
                    if (GUILayout.Button("Add new default scale"))
                        model.DefaultScales.Add(newScale);

                    newScale = EditorGUILayout.FloatField(newScale);
                }
                EditorGUILayout.EndHorizontal();

                GUI.enabled = model.DefaultScales.Count > 1; //Need at least one
                    if (GUILayout.Button("Remove last default scale"))
                        model.DefaultScales.RemoveAt(model.DefaultScales.Count - 1);
                GUI.enabled = true;

                GUILayout.Label("Default scales:");
                EditorGUILayout.BeginHorizontal("Box");
                {
                    for(int i = 0; i < model.DefaultScales.Count; i++)
                        model.DefaultScales[i] = EditorGUILayout.FloatField(model.DefaultScales[i], GUILayout.ExpandWidth(false));
                }
                EditorGUILayout.EndHorizontal();


            }
            EditorGUILayout.EndVertical();
        }

    }
}
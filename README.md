## Note: Not production ready code

### What it is
This is a very simple [Unity](www.unity.com) package, it makes it possible to import assets directly from figma into unity.

### How to install
Just install it like any other git Unity package.

- Inside Unity, open the `Package Manager` from the menu item `Window`.
- In the package manager click on the plus icon in the top left corner
- Click on the option `Add package from git URL...`
- Paste the URL from this repo, with the desired version tag e.g.
`git@bitbucket.org:AndreasBL/figma2unitypackage.git#0.9.0`
- Unity will then import the package into your project.

### How to use it
Open the editor window from window -> Figma 2 Unity.

- Get a figma token from: https://www.figma.com/developers/api#access-tokens
- In your browser, open one of your figma projects. Copy / paste the project ID from the URL.
- If you feel brave, open the the `backend` in the "Figma 2 Unity" window.
- You are on your own.. :) More doc will come in version 1.0.0

### Pull Requests
Pull requests are more than welcome! 

**Current version: 0.9.0**\
Note: I don't consider this stable before version 1.0.0.

### Roadmap
#### Version 0.9.0
- Support an unity editor workflow only 
- Editor window, where the user can define assets, so they are easy to pull into the project.
- Update individual assets.
- Fetch asset overview from backend (Only works for small projects)

##### Known limitations
- UI sucks
- No error handling.

#### Version 1.0.0
- Refactor editor UI to Unity UI Toolkit
- Fetch data for a single node. So larger projects becomes useable. 

#### Version 2.0.0
- Support fetching assets runtime
    - Only work in dev builds

#### Ideas
- Runtime: Long press or other interaction on sprite to fetch on new from backend.